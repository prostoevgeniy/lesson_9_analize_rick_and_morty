console.log('Lesson 9 is here...');

axios
    .get('https://rickandmortyapi.com/api/character/',)
    .then(res => {

        const {pages} = res.data.info

        Promise.all([... new Array(pages)].map((el, ind) => axios.get(`https://rickandmortyapi.com/api/character/?page=${ind+1}`)))
            .then(res => {
                const arr = [], episodesArr = [], originEath = {}, numberOfParticipantsInEpisodes = {};
                res.forEach((el, ind) => {
//                    console.log(el.data.results);
                    el.data.results.forEach((el, ind) => {
                    	// 
                        el.origin.name.toLowerCase().indexOf('earth') != -1 ? 
                        arr.push(originEath[el.name] = {...el}) : null;

                        el.episode.forEach((elem, ind) => {  
                            Array.isArray(numberOfParticipantsInEpisodes[elem]) ? numberOfParticipantsInEpisodes[elem].push({...el}) :
                            numberOfParticipantsInEpisodes[elem] = [];
                        })  
                    })
                } )
                console.log('characters of origin from Earth');
                console.log(originEath);
                console.log('group characters by episode');
                console.log(numberOfParticipantsInEpisodes);
            })
    })
